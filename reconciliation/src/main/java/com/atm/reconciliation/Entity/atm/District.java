package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "district")
public class District extends AbstractName {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "district_Seq_GEN")
    @SequenceGenerator(name = "district_GEN", sequenceName = "district_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "province_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_provinceId_District"))
    private Province provinceId;

}
