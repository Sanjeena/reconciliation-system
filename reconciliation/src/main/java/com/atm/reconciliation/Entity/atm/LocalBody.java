package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "local_body")
public class LocalBody extends AbstractName {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "local_body_Seq_GEN")
    @SequenceGenerator(name = "local_body_GEN", sequenceName = "local_body_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "district_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_districtId_LocalBody"))
    private District districtId;


    @ManyToOne
    @NotNull
    @JoinColumn(name = "local_body_type_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_localBodyTypeId_LocalBody"))
    private LocalBodyType localBodyTypeId;


}
