package com.atm.reconciliation.Entity.configuration;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "week_day")
public class WeekDay extends AbstractName {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "week_day_Seq_GEN")
    @SequenceGenerator(name = "week_day_GEN", sequenceName = "week_day_Seq", initialValue = 1, allocationSize = 1)
    private Long id;
}
