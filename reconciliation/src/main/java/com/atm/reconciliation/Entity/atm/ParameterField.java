package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "parameter_field")
public class ParameterField extends AbstractName {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "parameter_field_Seq_GEN")
    @SequenceGenerator(name = "parameter_field_GEN", sequenceName = "parameter_field_Seq", initialValue = 1, allocationSize = 1)
    private Long id;
}
