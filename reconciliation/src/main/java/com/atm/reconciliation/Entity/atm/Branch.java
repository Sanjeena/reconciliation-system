package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name ="branch")
public class Branch extends AbstractName {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "branch_Seq_GEN")
    @SequenceGenerator(name = "branch_GEN", sequenceName = "branch_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "local_body_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_localBodyId_Branch"))
    private LocalBody localBodyId;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "office_type_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_officeTypeId_Branch"))
    private OfficeType officeTypeId;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "branch_class_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_branchClassCategoryId_Branch"))
    private BranchClassCategory branchClassCategoryId;


}
