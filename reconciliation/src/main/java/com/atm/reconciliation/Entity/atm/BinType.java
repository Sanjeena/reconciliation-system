package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name ="bin_type")
public class BinType extends AbstractName {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bin_type_Seq_GEN")
    @SequenceGenerator(name = "bin_type_GEN", sequenceName = "bin_type_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

}
