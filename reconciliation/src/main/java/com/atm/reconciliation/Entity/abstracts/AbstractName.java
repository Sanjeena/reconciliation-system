package com.atm.reconciliation.Entity.abstracts;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractName extends AbstractEntity<Long> {

    @Column(name = "NAME", nullable = false, columnDefinition = "VARCHAR(250)")
    private String name;
}
