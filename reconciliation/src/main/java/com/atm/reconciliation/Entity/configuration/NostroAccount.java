package com.atm.reconciliation.Entity.configuration;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import com.atm.reconciliation.Entity.atm.Currency;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "nostro_account")
public class NostroAccount extends AbstractName {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "nostro_account_Seq_GEN")
    @SequenceGenerator(name = "nostro_account_GEN", sequenceName = "nostro_account_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @NotNull
    @Column(columnDefinition="VARCHAR(25)")
    private String account_number;

    @ManyToOne
    @JoinColumn(name = "currency_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_currencyId_NostroAccount"))
    private Currency currencyId;

    @Column(precision = 20, scale = 2)
    private double statement_balance;

    @Column(precision = 20, scale = 2)
    private double fcy_balance;

    @Column(precision = 20, scale = 2)
    private double icy_balance;

    @Temporal(TemporalType.TIMESTAMP)
    private Date last_update_date;

}
