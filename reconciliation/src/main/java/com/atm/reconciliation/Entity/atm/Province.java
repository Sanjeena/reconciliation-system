package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "province")
public class Province extends AbstractName {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "province_Seq_GEN")
    @SequenceGenerator(name = "province_GEN", sequenceName = "province_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "country_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_countryId_Province"))
    private Country countryId;


}
