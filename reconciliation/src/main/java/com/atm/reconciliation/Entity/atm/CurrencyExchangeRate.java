package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractEntity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@Table(name = "currency_exchange_rate")
public class CurrencyExchangeRate extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "currency_exchange_rate_Seq_GEN")
    @SequenceGenerator(name = "currency_exchange_rate_GEN", sequenceName = "currency_exchange_rate_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "source_currency_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_sourceCurrencyId_CurrencyExchangeRate"))
    private Currency sourceCurrencyId;

//    @ManyToOne
//    @NotNull
//    @JoinColumn(name = "source_currency_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_targetCurrencyId_CurrencyExchangeRate"))
//    private Currency targetCurrencyId;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date tradingDate;

     @NotNull
    private double averageRate;

     @NotNull
    private double closingRate;

}
