package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "transaction_type")
public class TransactionType extends AbstractName {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transaction_type_Seq_GEN")
    @SequenceGenerator(name = "transaction_type_GEN", sequenceName = "transaction_type_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

}
