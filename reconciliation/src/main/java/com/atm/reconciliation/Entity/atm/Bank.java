package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "bank")
public class Bank extends AbstractName {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bank_Seq_GEN")
    @SequenceGenerator(name = "bank_Seq_GEN", sequenceName = "bank_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @Column(columnDefinition = "VARCHAR(250)")
    private String code;
}
