package com.atm.reconciliation.Entity.security;

import com.atm.reconciliation.Entity.abstracts.AbstractEntity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "role_module_privilege")
public class RoleModulePrivilege extends AbstractEntity<Long> {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_module_privilege_Seq_GEN")
    @SequenceGenerator(name = "role_module_privilege_Seq_GEN", sequenceName = "role_module_privilege_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_roleId_RoleModulePrivilege"))
    private Roles roleId;

    @ManyToOne
    @JoinColumn(name = "module_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_moduleId_RoleModulePrivilege"))
    private Module moduleId;

    @ManyToOne
    @JoinColumn(name = "privilege_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_privilegeId_RoleModulePrivilege"))
    private Privilege privilegeId;
}
