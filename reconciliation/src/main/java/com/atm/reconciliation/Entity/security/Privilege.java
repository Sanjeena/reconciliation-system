package com.atm.reconciliation.Entity.security;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "privilege")
public class Privilege extends AbstractName {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "privilege_Seq_GEN")
    @SequenceGenerator(name = "privilege_Seq_GEN", sequenceName = "privilege_Seq", initialValue = 1, allocationSize = 1)
    private Long id;
}
