package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractEntity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name="white_listed_ip")
public class WhiteListedIp extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "white_listed_ip_Seq_GEN")
    @SequenceGenerator(name = "white_listed_ip_GEN", sequenceName = "white_listed_ip_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @NotNull
    @Column(columnDefinition = "VARCHAR(15)")
    private String address;

    @Column(columnDefinition = "VARCHAR(250)")
    private String user_description;
}
