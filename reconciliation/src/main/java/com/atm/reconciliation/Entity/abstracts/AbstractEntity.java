package com.atm.reconciliation.Entity.abstracts;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractEntity<PK extends Serializable> {
    private static final long serialVersionUID = 8453654076725018243L;
    @Basic
    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false)
    private Date createdDate;
    @Basic
    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedDate;

    @Basic
    @CreatedBy
    @Column(updatable = false)
    private Long createdBy;

    @Basic
    @LastModifiedBy
    private Long modifiedBy;

//    private Status status=ACTIVE;
}
