package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "local_body_type")
public class LocalBodyType extends AbstractName {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "local_body_type_Seq_GEN")
    @SequenceGenerator(name = "local_body_type_GEN", sequenceName = "local_body_type_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    private int local_body_level;

}
