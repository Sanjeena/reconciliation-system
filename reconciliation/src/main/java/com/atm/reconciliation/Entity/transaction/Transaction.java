package com.atm.reconciliation.Entity.transaction;

import com.atm.reconciliation.Entity.abstracts.AbstractEntity;
import com.atm.reconciliation.Entity.atm.TransactionDataSource;
import com.atm.reconciliation.Entity.atm.TransactionType;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "transaction")
public class Transaction extends AbstractEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transaction_Seq_GEN")
    @SequenceGenerator(name = "transaction_Seq_GEN", sequenceName = "transaction_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "transaction_type_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_transactionTypeId_Transaction"))
    private TransactionType transactionTypeId;

    @ManyToOne
    @JoinColumn(name = "transaction_data_source_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_transactionDataSourceId_Transaction"))
    private TransactionDataSource transactionDataSourceId;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date transaction_date;

    @NotNull
    @Column(precision = 20,scale = 2)
    private double amount;

    private String posting_type;

    private String description;


}
