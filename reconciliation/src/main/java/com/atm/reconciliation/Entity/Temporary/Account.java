package com.atm.reconciliation.Entity.Temporary;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "account")
public class Account extends AbstractName {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_Seq_GEN")
    @SequenceGenerator(name = "account_Seq_GEN", sequenceName = "account_Seq", initialValue = 1, allocationSize = 1)
    private Long id;
}
