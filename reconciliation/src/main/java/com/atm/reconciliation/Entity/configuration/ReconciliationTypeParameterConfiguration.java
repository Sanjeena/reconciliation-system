package com.atm.reconciliation.Entity.configuration;

import com.atm.reconciliation.Entity.atm.ParameterField;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "reconciliation_type_parameter_configuration")
public class ReconciliationTypeParameterConfiguration {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reconciliation_type_parameter_configuration_Seq_GEN")
    @SequenceGenerator(name = "reconciliation_type_parameter_configuration_GEN", sequenceName = "reconciliation_type_parameter_configuration_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "reconciliation_type", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_reconciliationTypeId_ReconciliationTypeParameterConfiguration"))
    private ReconciliationType reconciliationTypeId;

    @ManyToOne
    @JoinColumn(name = "parameter_field_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_parameterFieldId_ReconciliationTypeParameterConfiguration"))
    private ParameterField parameterFieldId;
}
