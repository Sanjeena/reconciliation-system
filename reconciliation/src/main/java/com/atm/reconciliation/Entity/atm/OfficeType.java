package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "office_type")
public class OfficeType extends AbstractName {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "office_type_Seq_GEN")
    @SequenceGenerator(name = "office_type_GEN", sequenceName = "office_type_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    private int office_type_level;
}
