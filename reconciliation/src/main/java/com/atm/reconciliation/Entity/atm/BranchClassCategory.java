package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "branch_class_category")
public class BranchClassCategory extends AbstractName {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "branch_class_category_Seq_GEN")
    @SequenceGenerator(name = "branch_class_category_GEN", sequenceName = "branch_class_category_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

}
