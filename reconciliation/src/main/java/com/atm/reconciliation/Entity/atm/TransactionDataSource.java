package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "transaction_data_source")
public class TransactionDataSource extends AbstractName {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transaction_data_source_Seq_GEN")
    @SequenceGenerator(name = "transaction_data_source_GEN", sequenceName = "transaction_data_source_Seq", initialValue = 1, allocationSize = 1)
    private Long id;
}
