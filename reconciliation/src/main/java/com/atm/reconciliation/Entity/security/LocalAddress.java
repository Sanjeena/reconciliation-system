package com.atm.reconciliation.Entity.security;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "local_address")
public class LocalAddress extends AbstractName {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "local_address_Seq_GEN")
    @SequenceGenerator(name = "local_address_Seq_GEN", sequenceName = "local_address_Seq", initialValue = 1, allocationSize = 1)
    private Long id;
}
