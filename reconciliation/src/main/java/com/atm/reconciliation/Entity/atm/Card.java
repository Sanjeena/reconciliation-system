package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractEntity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "card")
public class Card extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "card_Seq_GEN")
    @SequenceGenerator(name = "card_GEN", sequenceName = "card_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "bank_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_bankId_Card"))
    private Bank bankId;

    private String card_no;

    private String card_holder_name;

    @Temporal(TemporalType.TIMESTAMP)
    private Date valid_from;

    @Temporal(TemporalType.TIMESTAMP)
    private Date valid_till;
}
