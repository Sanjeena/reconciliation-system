package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "terminal_type")
public class TerminalType extends AbstractName {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "terminal_type_Seq_GEN")
    @SequenceGenerator(name = "terminal_type_GEN", sequenceName = "terminal_type_Seq", initialValue = 1, allocationSize = 1)
    private Long id;
}
