package com.atm.reconciliation.Entity.security;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "module")
public class Module extends AbstractName {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "module_Seq_GEN")
    @SequenceGenerator(name = "module_Seq_GEN", sequenceName = "module_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    private  int parent_id;
}
