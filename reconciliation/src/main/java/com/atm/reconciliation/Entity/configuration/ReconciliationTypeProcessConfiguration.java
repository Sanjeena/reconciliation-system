package com.atm.reconciliation.Entity.configuration;

import com.atm.reconciliation.Entity.atm.TransactionDataSource;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "reconciliation_type_process_configuration")
public class ReconciliationTypeProcessConfiguration {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reconciliation_type_process_configuration_Seq_GEN")
    @SequenceGenerator(name = "reconciliation_type_process_configuration_GEN", sequenceName = "reconciliation_type_process_configuration_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "reconciliation_type", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_reconciliationTypeId_ReconciliationTypeProcessConfiguration"))
    private ReconciliationType reconciliationTypeId;

    @ManyToOne
    @JoinColumn(name = "transaction_data_source_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_transactionDataSourceId_ReconciliationTypeProcessConfiguration"))
    private TransactionDataSource transactionDataSourceId;
}
