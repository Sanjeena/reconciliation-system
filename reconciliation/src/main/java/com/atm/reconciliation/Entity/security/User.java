package com.atm.reconciliation.Entity.security;

import com.atm.reconciliation.Entity.abstracts.AbstractEntity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "user")
public class User extends AbstractEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_Seq_GEN")
    @SequenceGenerator(name = "user_Seq_GEN", sequenceName = "user_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @NotNull
    @Column(columnDefinition = "VARCHAR(250)")
    private String username;

    @Column(columnDefinition = "VARCHAR(250)")
    private String email;

    @Column(columnDefinition = "VARCHAR(250)")
    private String email_confirmed;

    @Column(columnDefinition = "VARCHAR(250)")
    private String password_hash;

    @Column(columnDefinition = "VARCHAR(250)")
    private String phone_number;

    @Column(columnDefinition = "VARCHAR(250)")
    private String phone_number_confirmed;

    private boolean two_factor_enabled;

    @Temporal(TemporalType.TIMESTAMP)
    private Date lockout_end;

    private boolean lockout_enabled;

    @Column(columnDefinition = "UNSIGNED INT(2)")
    private int access_failed_count;

    @NotNull
    @Column(columnDefinition="VARCHAR(250)")
    private String first_name;

    @Column(columnDefinition="VARCHAR(250)")
    private String middle_name;

    @NotNull
    @Column(columnDefinition="VARCHAR(250)")
    private String last_name;

    @ManyToOne
    @JoinColumn(name = "local_address_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_localAddressId_User"))
    private LocalAddress localAddressId;

//    private Status status;

    @Temporal(TemporalType.TIMESTAMP)
    private Date status_change_date;

    private String status_change_user_id;

    private String user_access_status;

    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_roleId_User"))
    private Roles roleId;

}
