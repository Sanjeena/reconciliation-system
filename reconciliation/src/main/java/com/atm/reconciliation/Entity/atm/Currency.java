package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "currency")
public class Currency extends AbstractName
{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "currency_Seq_GEN")
    @SequenceGenerator(name = "currency_GEN", sequenceName = "currency_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @NotNull
    @Column(name = "code",columnDefinition = "VARCHAR(3)")
    private String code;

    @NotNull
    @Column(name = "iso_code",columnDefinition = "VARCHAR(3)")
    private String isoCode;

    @Column(name = "sign",columnDefinition = "VARCHAR(1)")
    private String sign;

    @NotNull
    @Column(name = "description",columnDefinition = "VARCHAR(250)")
    private String description;
}
