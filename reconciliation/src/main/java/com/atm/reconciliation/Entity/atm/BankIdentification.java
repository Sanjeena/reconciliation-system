package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractEntity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "bank_identification")
public class BankIdentification extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bank_identification_Seq_GEN")
    @SequenceGenerator(name = "bank_identification_GEN", sequenceName = "bank_identification_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "bin_type_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_binTypeId_BankIdentification"), nullable = false)
    private BinType binTypeId;

    @ManyToOne
    @JoinColumn(name = "bank_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_bankId_BankIdentification"),nullable = false)
    private Bank bankId;

    @Column(columnDefinition = "VARCHAR(250)", nullable = false)
    private String bin;

}
