package com.atm.reconciliation.Entity.configuration;

import com.atm.reconciliation.Entity.abstracts.AbstractEntity;
import com.atm.reconciliation.Entity.atm.Periodicity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "reconciliation_import_scheduler")
public class ReconciliationImportScheduler extends AbstractEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reconciliation_import_scheduler_Seq_GEN")
    @SequenceGenerator(name = "reconciliation_import_scheduler_GEN", sequenceName = "reconciliation_import_scheduler_Seq", initialValue = 1, allocationSize = 1)
    private Long id;


    @ManyToOne
    @JoinColumn(name = "reconciliation_type_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_reconciliationTypeId_ReconciliationImportScheduler"))
    private ReconciliationType reconciliationTypeId;

    @ManyToOne
    @JoinColumn(name = "periodicity_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_periodicityId_ReconciliationImportScheduler"))
    private Periodicity periodicityId;

    @ManyToOne
    @JoinColumn(name = "day_of_the_week", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_weekdayId_ReconciliationImportScheduler"))
    private WeekDay weekdayId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date time;


}
