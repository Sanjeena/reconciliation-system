package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "country")
public class Country extends AbstractName {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "country_Seq_GEN")
    @SequenceGenerator(name = "country_GEN", sequenceName = "country_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

}
