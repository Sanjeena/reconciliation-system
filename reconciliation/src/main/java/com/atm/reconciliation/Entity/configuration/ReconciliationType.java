package com.atm.reconciliation.Entity.configuration;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "reconciliation_type")
public class ReconciliationType extends AbstractName {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reconciliation_type_Seq_GEN")
    @SequenceGenerator(name = "reconciliation_type_GEN", sequenceName = "reconciliation_type_Seq", initialValue = 1, allocationSize = 1)
    private Long id;
}
