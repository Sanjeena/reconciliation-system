package com.atm.reconciliation.Entity.security;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "roles")
public class Roles extends AbstractName {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "roles_Seq_GEN")
    @SequenceGenerator(name = "roles_Seq_GEN", sequenceName = "roles_Seq", initialValue = 1, allocationSize = 1)
    private Long id;
}
