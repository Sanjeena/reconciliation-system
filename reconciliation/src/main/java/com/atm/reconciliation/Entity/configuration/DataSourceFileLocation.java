package com.atm.reconciliation.Entity.configuration;

import com.atm.reconciliation.Entity.abstracts.AbstractEntity;
import com.atm.reconciliation.Entity.atm.TransactionDataSource;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "data_source_file_location")
public class DataSourceFileLocation extends AbstractEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "data_source_file_location_Seq_GEN")
    @SequenceGenerator(name = "data_source_file_location_GEN", sequenceName = "data_source_file_location_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "reconciliation_type_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_reconciliationTypeId_DataSourceFileLocation"))
    private ReconciliationType reconciliationTypeId;

    @ManyToOne
    @JoinColumn(name = "transaction_data_source_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_transactionDataSourceId_DataSourceFileLocation"))
    private TransactionDataSource transactionDataSourceId;

    @NotNull
    @Column(columnDefinition="VARCHAR(25)")
    private String ftp_path;
}
