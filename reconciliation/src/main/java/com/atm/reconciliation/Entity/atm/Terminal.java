package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name = "terminal")
public class Terminal extends AbstractName {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "terminal_Seq_GEN")
    @SequenceGenerator(name = "terminal_GEN", sequenceName = "terminal_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "bank_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_bankId_Terminal"))
    private Bank bankId;

    @ManyToOne
    @JoinColumn(name = "vendor_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_vendorId_Terminal"))
    private Vendor vendorId;

    @ManyToOne
    @JoinColumn(name = "switch_id", referencedColumnName = "Id",foreignKey = @ForeignKey(name = "FK_switchId_Terminal"))
    private Switch switchId;
}
