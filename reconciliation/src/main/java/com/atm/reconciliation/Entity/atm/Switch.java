package com.admin.spring.Entity.atm;

import com.admin.spring.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name="switch")
public class Switch extends AbstractName {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "switch_Seq_GEN")
    @SequenceGenerator(name = "switch_GEN", sequenceName = "switch_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

}
