package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "fiscal_year")
public class FiscalYear extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fiscal_year_Seq_GEN")
    @SequenceGenerator(name = "fiscal_year_GEN", sequenceName = "fiscal_year_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @NotNull
    @Column(name = "fiscal_year", columnDefinition = "VARCHAR(250)")
    private String fiscal_year;

    @Temporal(TemporalType.TIMESTAMP)
    private Date start_date;

    @Temporal(TemporalType.TIMESTAMP)
    private Date end_date;

}
