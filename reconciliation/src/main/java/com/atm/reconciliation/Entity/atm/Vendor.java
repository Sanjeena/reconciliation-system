package com.atm.reconciliation.Entity.atm;

import com.atm.reconciliation.Entity.abstracts.AbstractName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@Table(name="vendor")
public class Vendor extends AbstractName {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vendor_Seq_GEN")
    @SequenceGenerator(name = "vendor_GEN", sequenceName = "vendor_Seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @NotNull
    private String file_location;

}
